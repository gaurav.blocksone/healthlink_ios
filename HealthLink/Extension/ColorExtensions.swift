//
//  ColorExtensions.swift
//  HealthLink
//
//  Created by Gaurav on 15/05/23.
//

import Foundation
import UIKit

extension UIColor {
    static func themeColor(offset: CGFloat) -> UIColor {
        return UIColor(red: 209.0/255.0, green: 4.0/255.0, blue: 3.0/255.0, alpha: offset)
        //return UIColor(red: 93.0/255.0, green: 52.0/255.0, blue: 170.0/255.0, alpha: offset)
    }
    
    static func textfieldLayerColor(with offset: CGFloat)  -> UIColor {
        return UIColor(red: 26.0/255.0, green: 26.0/255.0, blue: 26.0/255.0, alpha: offset)
    }
    
    static func subheaderLabelColor(with offset: CGFloat)  -> UIColor {
        return UIColor(red: 34.0/255.0, green: 34.0/255.0, blue: 34.0/255.0, alpha: offset)
    }
    
    static func tabBarColor(with offset: CGFloat)  -> UIColor {
        return UIColor(red: 90.0/255.0, green: 90.0/255.0, blue: 90.0/255.0, alpha: offset)
    }
    
    static func datePickerColor(with offset: CGFloat) -> UIColor {
        return UIColor(red: 200/255, green: 202/255, blue: 215/255, alpha: offset)
    }
    
    static func segmentColor(with offset: CGFloat) -> UIColor {
        return UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: offset)
    }
    
    static func lightGray(with offset: CGFloat) -> UIColor {
        return UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: offset)
    }
    
    static func commentBgColor(with offset: CGFloat) -> UIColor {
        return UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: offset)
    }
}


extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    static func colorFrom(hex: String) -> UIColor {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hex)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return UIColor(rgb: Int(hexInt))
    }
}

extension UIColor {
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
    }
}

extension UIColor {
    func as1ptImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        guard let ctx = UIGraphicsGetCurrentContext() else { return UIImage() }
        self.setFill()
        ctx.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else { return UIImage() }
        UIGraphicsEndImageContext()
        return image
    }
}

extension UIColor {
    func image(_ size: CGSize = CGSize(width: 1, height: 1)) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { rendererContext in
            self.setFill()
            rendererContext.fill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        }
    }
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
    func lighter(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: abs(percentage) )
    }
    
    func darker(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: -1 * abs(percentage) )
    }
    
    func adjust(by percentage: CGFloat = 30.0) -> UIColor? {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return UIColor(red: min(red + percentage/100, 1.0),
                           green: min(green + percentage/100, 1.0),
                           blue: min(blue + percentage/100, 1.0),
                           alpha: alpha)
        } else {
            return nil
        }
    }
}
