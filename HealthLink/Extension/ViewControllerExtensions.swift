//
//  ViewControllerExtensions.swift
//  HealthLink
//
//  Created by Gaurav on 15/05/23.
//

import Foundation
import UIKit

extension UIWindow {
    static var rootKey: UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.windows.first { $0.isKeyWindow }
        } else {
            return UIApplication.shared.keyWindow
        }
    }
}
//MARK:- Get Current View controller
extension UIApplication {
    class func topViewController(controller: UIViewController? = UIWindow.rootKey?.rootViewController) -> UIViewController? {
        if let tabController = controller as? UITabBarController {
            return topViewController(controller: tabController.selectedViewController)
        }
        if let navController = controller as? UINavigationController {
            return topViewController(controller: navController.visibleViewController)
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
