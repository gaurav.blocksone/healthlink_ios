//
//  Constants.swift
//  HealthLink
//
//  Created by Gaurav on 15/05/23.
//

import Foundation
import UIKit


//MARK: - Devices
struct Device {
    let SCREEN_BOUND = UIScreen.main.bounds
    static let IS_IPAD             = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA           = UIScreen.main.scale >= 2.0
    
    static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_BOUND        = UIScreen.main.bounds
    
    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  <= 568
    static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_6P_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH <= 736
    static let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH == 812
    
    static let IS_IPAD_9           = IS_IPAD && SCREEN_MAX_LENGTH == 1024
    static let IS_IPHONE_XR         = IS_IPHONE && SCREEN_MAX_LENGTH == 896
    
    static let IS_IPHONE_8         = IS_IPHONE && SCREEN_MAX_LENGTH > 568  && SCREEN_MAX_LENGTH <= 667
}

//MARK:- All defulats
struct DefaultConstants {
    static let GSignInClientID = "844495434146-unosrkl4gn99t90t8p1abe11v6fmovl4.apps.googleusercontent.com"
}

//MARK: - Fonts
enum FontBook: String {
    
    case RobotoBold = "Roboto-Bold"
    case RobotoBoldItalic = "Roboto-BoldItalic"
    case RobotoItalic = "Roboto-Italic"
    case RobotoLight = "Roboto-Light"
    case RobotoLightItalic = "Roboto-LightItalic"
    case RobotoMedium = "Roboto-Medium"
    case RobotoMediumItalic = "Roboto-MediumItalic"
    case RobotoRegular = "Roboto-Regular"
    case RobotoThin = "Roboto-Thin"
    case RobotoThinItalic = "Roboto-ThinItalic"
    case RobotoBlack = "Roboto-Black"
    case RobotoBlackItalic = "Roboto-BlackItalic"
    case YellowtailRegular = "Yellowtail-Regular"
    
    func of(size: CGFloat) -> UIFont {
        if let font = UIFont(name: self.rawValue, size: size) {
            return font
        }
        return UIFont.boldSystemFont(ofSize: size)
    }
}

struct CustomColors {
    
    static let appThemeColor = UIColor.rgb(red: 76, green: 76, blue: 220, alpha: 1)

    static let darkGreyColor = UIColor.rgb(red: 51, green: 51, blue: 51, alpha: 1)
    static let darkBlueColor = UIColor.rgb(red: 28, green: 80, blue: 163, alpha: 1)
    static let darkRedColor = UIColor.rgb(red: 235, green: 18, blue: 47, alpha: 1)
    static let lightGreyColor = UIColor.rgb(red: 214, green: 214, blue: 214, alpha: 1)
    static let normalGreyColor = UIColor.rgb(red: 136, green: 136, blue: 136, alpha: 1)

    static let appBlueColor = UIColor(hexString:"#1C50A3")
    static let appRedColor = UIColor(hexString:"#EB122F")
    static let customRedColor = UIColor(hexString:"#C22446")
    
    
}
