//
//  NoInternetViewController.swift
//  HealthLink
//
//  Created by Gaurav on 17/05/23.
//

import Foundation
import UIKit

class NoInternetViewController: UIViewController{
    @IBOutlet weak var retryButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func retryButtonAction(_ sender: Any) {
    }
}
