//
//  ViewController.swift
//  HealthLink
//
//  Created by Gaurav on 12/05/23.
//

import UIKit
import GoogleSignIn

class OnBoardingViewController: UIViewController, NoInternetDelegate {
    func retryAgain() {
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func googleButtonAction(_ sender: Any) {
        if !NetworkStatus.shared.isPresent() {
            NoInternetClass.shared.delegate = self
            NoInternetClass.shared.show()
            return
        }
    }
    
    
}

