//
//  SignUPViewCOntroller.swift
//  HealthLink
//
//  Created by Gaurav on 19/05/23.
//

import Foundation
import UIKit
import GoogleSignIn

class SignUPViewCOntroller: UIViewController, NoInternetDelegate{
    func retryAgain() {
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func googleButtonAction(_ sender: Any) {
        if !NetworkStatus.shared.isPresent() {
            NoInternetClass.shared.delegate = self
            NoInternetClass.shared.show()
            return
        }
        loginToGoogle()
    }
    
    private func loginToGoogle(){
        let config = GIDConfiguration(clientID: DefaultConstants.GSignInClientID)
        
//        GIDSignIn.sharedInstance.signIn(with: config, presenting: self){ data, error in
//            if error != nil{
//                HealthLinkAlertView.shared.show(body: error?.localizedDescription ?? "", dimMode: true, presentationStyle: .top)
//                return
//            }else{
//                self.createGoogleRequest(data)
//            }
//        }
    }
}
