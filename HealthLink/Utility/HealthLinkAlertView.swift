//
//  HealthLinkAlertView.swift
//  HealthLink
//
//  Created by Gaurav on 19/05/23.
//

import Foundation
import SwiftMessages

//Delegate for button action
protocol selectAllButtonDelegate:class {
    func buttonPressed(result:Bool)
}

class HealthLinkAlertView: NSObject {
    
    static let shared = HealthLinkAlertView()
    private let seconds = 2.0
    var delegate : selectAllButtonDelegate?
    var isValueChanged = false
    
    internal enum BasicPresentationStyle {
        case top
        case bottom
        case center
    }
    
    private var messageViewConfig: SwiftMessages.Config = {
        var config = SwiftMessages.defaultConfig
        config.interactiveHide = false
        config.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        return config
    }()
    
    // MARK: - Alerts Methods
    public func warningMessage(title: String, body: String, dimMode: Bool, presentationStyle: BasicPresentationStyle) {
        DispatchQueue.main.async {
            let warning = MessageView.viewFromNib(layout: .cardView)
            warning.configureTheme(.warning)
            warning.configureDropShadow()
            warning.configureContent(title: title, body: body)
            warning.button?.isHidden = true
            warning.iconImageView?.isHidden = true
            
            var warningConfig = SwiftMessages.defaultConfig
            if presentationStyle == .top {
                warningConfig.presentationStyle = .top
            } else if presentationStyle == .bottom {
                warningConfig.presentationStyle = .bottom
            } else if presentationStyle == .center {
                warningConfig.presentationStyle = .center
            }
            if dimMode {
                warningConfig.dimMode = .blur(style: .regular, alpha: 1.0, interactive: true)
            }
            warningConfig.duration = .seconds(seconds: self.seconds)
            warningConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            SwiftMessages.show(config: warningConfig, view: warning)
        }
    }
    
    public func errorMessage(title: String, body: String, dimMode: Bool, presentationStyle: BasicPresentationStyle) {
        DispatchQueue.main.async {
            let error = MessageView.viewFromNib(layout: .cardView)
            error.configureTheme(.error)
            error.configureDropShadow()
            error.configureContent(title: title, body: body)
            error.button?.isHidden = true
            error.iconImageView?.isHidden = true
            
            var errorConfig = SwiftMessages.defaultConfig
            if presentationStyle == .top {
                errorConfig.presentationStyle = .top
            } else if presentationStyle == .bottom {
                errorConfig.presentationStyle = .bottom
            } else if presentationStyle == .center {
                errorConfig.presentationStyle = .center
            }
            if dimMode {
                errorConfig.dimMode = .blur(style: .light, alpha: 1.0, interactive: true)
            }
            errorConfig.duration = .seconds(seconds: self.seconds)
            errorConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            SwiftMessages.show(config: errorConfig, view: error)
        }
    }
    
    public func infoMessage(title: String, body: String, dimMode: Bool, presentationStyle: BasicPresentationStyle) {
        DispatchQueue.main.async {
            let info = MessageView.viewFromNib(layout: .cardView)
            info.configureTheme(.info)
            info.configureDropShadow()
            info.configureContent(title: title, body: body)
            info.button?.isHidden = true
            info.iconImageView?.isHidden = true
            
            var inforConfig = SwiftMessages.defaultConfig
            if presentationStyle == .top {
                inforConfig.presentationStyle = .top
            } else if presentationStyle == .bottom {
                inforConfig.presentationStyle = .bottom
            } else if presentationStyle == .center {
                inforConfig.presentationStyle = .center
            }
            if dimMode {
                inforConfig.dimMode = .blur(style: .regular, alpha: 1.0, interactive: true)
            }
            inforConfig.duration = .seconds(seconds: self.seconds)
            inforConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            SwiftMessages.show(config: inforConfig, view: info)
        }
    }
    
    public func successMessage(title: String, body: String, dimMode: Bool, presentationStyle: BasicPresentationStyle) {
        DispatchQueue.main.async {
            let success = MessageView.viewFromNib(layout: .cardView)
            success.configureTheme(.success)
            success.configureDropShadow()
            success.configureContent(title: title, body: body)
            success.button?.isHidden = true
            success.iconImageView?.isHidden = true
            
            var successConfig = SwiftMessages.defaultConfig
            if presentationStyle == .top {
                successConfig.presentationStyle = .top
            } else if presentationStyle == .bottom {
                successConfig.presentationStyle = .bottom
            } else if presentationStyle == .center {
                successConfig.presentationStyle = .center
            }
            if dimMode {
                successConfig.dimMode = .color(color: UIColor(white: 0, alpha: 0.1), interactive: true)
            }
            successConfig.duration = .seconds(seconds: self.seconds)
            successConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            SwiftMessages.show(config: successConfig, view: success)
        }
    }
    
    //MARK:- Used One
    public func showSelectionMsg(title: String = "", body: String, dimMode: Bool, presentationStyle: BasicPresentationStyle,isSelected:Bool) {
        DispatchQueue.main.async {
            let success = MessageView.viewFromNib(layout: .cardView)
            // success.backgroundColor = UIColor.init(rgb: 0x2A96D2)
            // success.configureTheme(.info)
            success.configureTheme(backgroundColor:UIColor.init(rgb: 0x2A96D2) , foregroundColor: UIColor.white)
            success.configureContent(title: title, body: body)
            //success.configureTheme(.info, iconStyle: .light)
            success.configureDropShadow()
            success.button?.backgroundColor = UIColor.init(rgb: 0x2A96D2)
            success.button?.isHidden = false
            success.button?.setTitle("Select All", for: .normal)
            success.button?.setTitleColor(.white, for: .normal)
            success.bodyLabel?.font = FontBook.RobotoMedium.of(size: 16)
            success.bodyLabel?.textColor = .white
            success.button?.titleLabel?.font = FontBook.RobotoMedium.of(size: 16)
            success.titleLabel?.textColor = .white
            success.titleLabel?.font = FontBook.RobotoMedium.of(size: 16)
            success.iconImageView?.image = UIImage(named:"cross")
            success.iconImageView?.isHidden = false
            self.isValueChanged = true
            success.button?.addTarget(self, action: #selector(self.selectAllButtonAction), for: .touchUpInside)
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction(sender:)))
            success.iconImageView?.isUserInteractionEnabled = true
            success.iconImageView?.addGestureRecognizer(tap)
            
            var successConfig = self.messageViewConfig
            if presentationStyle == .top {
                successConfig.presentationStyle = .top
            } else if presentationStyle == .bottom {
                successConfig.presentationStyle = .bottom
            } else if presentationStyle == .center {
                successConfig.presentationStyle = .center
            }
            if dimMode {
                successConfig.dimMode = .color(color: UIColor(white: 0, alpha: 0.3), interactive: true)//blur(style: .extraLight, alpha: 1.0, interactive: true)
            }
            successConfig.duration = .forever//.seconds(seconds: seconds)
            successConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            SwiftMessages.show(config: successConfig, view: success)
        }
    }
    
    //MARK:-Button Action
    @objc public func selectAllButtonAction(sender:UIButton){
//        DLog(message: "pressed..!!")
        delegate?.buttonPressed(result: true)
        //        if isValueChanged==true{
        //            //sender.titleLabel?.text = "DeSelect All"
        //        }else{
        //            sender.titleLabel?.text = "Select All"
        //            delegate?.buttonPressed(result: false)
        //        }
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        delegate?.buttonPressed(result: false)
    }
    
    //MARK:-
    
    
    //MARK:- Used One
    public func show(title: String = "", body: String, dimMode: Bool, presentationStyle: BasicPresentationStyle) {
        DispatchQueue.main.async {
            let success = MessageView.viewFromNib(layout: .cardView)
            success.configureTheme(.error)
            success.configureDropShadow()
            success.configureContent(title: title, body: body)
            success.button?.isHidden = true
            success.iconImageView?.isHidden = true
            
            var successConfig = self.messageViewConfig
            
            if presentationStyle == .top {
                successConfig.presentationStyle = .top
            } else if presentationStyle == .bottom {
                successConfig.presentationStyle = .bottom
            } else if presentationStyle == .center {
                successConfig.presentationStyle = .center
            }
            if dimMode {
                successConfig.dimMode = .color(color: UIColor(white: 0, alpha: 0.1), interactive: true)//blur(style: .extraLight, alpha: 1.0, interactive: true)
            }
            successConfig.duration = .seconds(seconds: self.seconds)
            successConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            SwiftMessages.show(config: successConfig, view: success)
        }
    }
    
    public func showOnController(title: String = "", body: String, dimMode: Bool, presentationStyle: BasicPresentationStyle) {
        DispatchQueue.main.async {
            let success = MessageView.viewFromNib(layout: .cardView)
            success.configureTheme(.error)
            success.configureDropShadow()
            success.configureContent(title: title, body: body)
            success.button?.isHidden = true
            success.iconImageView?.isHidden = true
            
            var successConfig = self.messageViewConfig
            if presentationStyle == .top {
                successConfig.presentationStyle = .top
            } else if presentationStyle == .bottom {
                successConfig.presentationStyle = .bottom
            } else if presentationStyle == .center {
                successConfig.presentationStyle = .center
            }
            if dimMode {
                successConfig.dimMode = .color(color: UIColor(white: 0, alpha: 0.3), interactive: true)//blur(style: .extraLight, alpha: 1.0, interactive: true)
            }
            successConfig.duration = .seconds(seconds: self.seconds)
            if let topcontroller = UIApplication.topViewController() {
                successConfig.presentationContext = .viewController(topcontroller)
                SwiftMessages.show(config: successConfig, view: success)
            }
        }
    }
}
