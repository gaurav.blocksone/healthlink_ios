//
//  NoInternetClass.swift
//  HealthLink
//
//  Created by Gaurav on 17/05/23.
//

import Foundation
import UIKit

protocol NoInternetDelegate: AnyObject {
    func retryAgain()
}

//MARK:- No internet class
class NoInternetClass: NSObject {
    static let shared = NoInternetClass()
    
    var delegate: NoInternetDelegate?
    let noInternetVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NoInternetViewController") as! NoInternetViewController
    
    func show() {
        //The below line is super important
    }
    
    @objc func dismissNoInternetView() {
        if NetworkStatus.shared.isPresent() {
            self.delegate?.retryAgain()
            self.noInternetVC.dismiss(animated: true, completion: nil)
        }
    }
}



